package com.example.android.courtcounter.util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.example.android.courtcounter.R;

/**
 * Created by yoprogramo on 1/24/2018.
 */

public final class AlertDialogUtil {


    private AlertDialogUtil() {
        //Hide Constructor
    }


    public static void showDialog(Context context, @Nullable String title, TextView textView) {
        showDialog(context, title, context.getResources().getString(R.string.ok), textView);
    }

    public static void showDialog(final Context context, @Nullable String title, String okMessage, final TextView textView) {


        if (context != null) {
            // get prompts.xml view
            LayoutInflater li = LayoutInflater.from(context);
            View promptsView = li.inflate(R.layout.prompts_layout, null);

            final AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle(title);
            builder.setView(promptsView);
            final TextView userInput = promptsView.findViewById(R.id.editTextDialogUserInput);
            userInput.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    userInput.setError(null);
                }
            });

            if (!TextUtils.isEmpty(title)) {
                builder.setTitle(title);
            }

            builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int i) {
                    dialog.dismiss();
                }
            });

            builder.setPositiveButton(okMessage,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                        }
                    });

            final AlertDialog dialog = builder.create();
            dialog.show();
            dialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (userInput.getText().toString().isEmpty()) {
                        userInput.setError(context.getString(R.string.field_required));
                    } else {
                        textView.setText(userInput.getText());
                        dialog.dismiss();
                    }

                }
            });

        }
    }

}
