package com.example.android.courtcounter;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.android.courtcounter.util.AlertDialogUtil;
import com.example.android.courtcounter.util.Constants;

public class MainActivity extends AppCompatActivity {


    TextView tvScoredTeamA;
    TextView tvScoredTeamB;
    TextView tvHomeTeam;
    TextView tvVisitorTeam;

    Button btnAddThreePointsA;
    Button btnAddTwoPointsA;
    Button btnAddOnePointA;

    Button btnAddThreePointsB;
    Button btnAddTwoPointsB;
    Button btnAddOnePointB;

    Button btnReset;

    private int scoreTeamA;
    private int scoreTeamB;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
        displayForTeamA(scoreTeamA);
        displayForTeamB(scoreTeamB);
    }

    private void initViews() {
        tvScoredTeamA = findViewById(R.id.team_a_score);
        tvScoredTeamB = findViewById(R.id.team_b_score);
        tvHomeTeam = findViewById(R.id.tv_home_team);
        tvVisitorTeam = findViewById(R.id.tv_visitor_team);
        btnAddThreePointsA = findViewById(R.id.btnAddThreePointsA);
        btnAddTwoPointsA = findViewById(R.id.btnAddTwoPointsA);
        btnAddOnePointA = findViewById(R.id.btnAddOnePointA);
        btnAddThreePointsB = findViewById(R.id.btnAddThreePointsB);
        btnAddTwoPointsB = findViewById(R.id.btnAddTwoPointsB);
        btnAddOnePointB = findViewById(R.id.btnAddOnePointB);
        btnReset = findViewById(R.id.btnReset);
        scoreTeamA = 0;
        scoreTeamB = 0;

        setOnClickListener();
    }

    private void setOnClickListener() {
        btnAddThreePointsA.setOnClickListener(onClickListener);
        btnAddTwoPointsA.setOnClickListener(onClickListener);
        btnAddOnePointA.setOnClickListener(onClickListener);
        btnAddThreePointsB.setOnClickListener(onClickListener);
        btnAddTwoPointsB.setOnClickListener(onClickListener);
        btnAddOnePointB.setOnClickListener(onClickListener);
        btnReset.setOnClickListener(onClickListener);
    }


    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            final int idBtn = view.getId();

            switch (idBtn) {
                case R.id.btnAddThreePointsA:
                    scoreTeamA = scoreTeamA + Constants.ADD_THREE_POINTS;
                    displayForTeamA(scoreTeamA);
                    break;
                case R.id.btnAddTwoPointsA:
                    scoreTeamA = scoreTeamA + Constants.ADD_TWO_POINTS;
                    displayForTeamA(scoreTeamA);
                    break;
                case R.id.btnAddOnePointA:
                    scoreTeamA = scoreTeamA + Constants.ADD_ONE_POINT;
                    displayForTeamA(scoreTeamA);
                    break;

                case R.id.btnAddThreePointsB:
                    scoreTeamB = scoreTeamB + Constants.ADD_THREE_POINTS;
                    displayForTeamB(scoreTeamB);
                    break;
                case R.id.btnAddTwoPointsB:
                    scoreTeamB = scoreTeamB + Constants.ADD_TWO_POINTS;
                    displayForTeamB(scoreTeamB);
                    break;
                case R.id.btnAddOnePointB:
                    scoreTeamB = scoreTeamB + Constants.ADD_ONE_POINT;
                    displayForTeamB(scoreTeamB);
                    break;
                case R.id.btnReset:
                    scoreTeamB = 0;
                    scoreTeamA = 0;
                    displayForTeamA(scoreTeamA);
                    displayForTeamB(scoreTeamB);
                    break;
            }
        }
    };

    /**
     * Displays the given score for Team A.
     */
    public void displayForTeamA(int score) {
        tvScoredTeamA.setText(String.valueOf(score));
    }

    /**
     * Displays the given score for Team B.
     */
    public void displayForTeamB(int score) {
        tvScoredTeamB.setText(String.valueOf(score));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Handle item selection
        switch (item.getItemId()) {
            case R.id.share:
                shareScore();
                return true;
            case R.id.set_teams_name:
                setTeams();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void setTeams() {
        AlertDialogUtil.showDialog(this, getString(R.string.visitor_team), tvVisitorTeam);
        AlertDialogUtil.showDialog(this, getString(R.string.home_team), tvHomeTeam);
    }

    private void shareScore() {
        String message = String.format("Game Score: %S: %d - %S: %d", tvHomeTeam.getText().toString(), scoreTeamA, tvVisitorTeam.getText().toString(), scoreTeamB);
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, message);
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, getResources().getString(R.string.share_score)));
    }
}
