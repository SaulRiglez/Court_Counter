package com.example.android.courtcounter.util;

/**
 * Created by yoprogramo on 1/24/2018.
 */

public final class Constants {

    public static final int ADD_THREE_POINTS = 3;
    public static final int ADD_TWO_POINTS = 2;
    public static final int ADD_ONE_POINT = 1;

    private Constants() {
        //Hide Constructor
    }
}
